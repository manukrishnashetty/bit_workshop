 
#include <Arduino.h>

 
#include "DHT.h"
 
#define DHTPIN 4 
#define DHTTYPE DHT11
DHT dht(DHTPIN, DHTTYPE);
// Provide the token generation process info.
 

 
const int RelayPIN = 26;
String Relay = "";
 
void setup()
{
 Serial.begin(9600);
 
 dht.begin();
 delay(2000);
}
 
void loop()
{
   float temp = dht.readTemperature();
   Serial.println(temp);
 
 delay(3000);
}

