#if defined(ESP32)
#include <WiFi.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#endif
#include <Firebase_ESP_Client.h>
 
// Provide the token generation process info.
#include "addons/TokenHelper.h"
// Provide the RTDB payload printing info and other helper functions.
#include "addons/RTDBHelper.h"
 
// Insert your network credentials
#define WIFI_SSID "devconfig1"
#define WIFI_PASSWORD "11111111"
 
// Insert Firebase project API Key
#define API_KEY "AIzaSyCDqVbQnFBasPGOtJ-uEmukOWnxGIPFRkg"
 
// Insert RTDB URLefine the RTDB URL */
#define DATABASE_URL "https://sampleiot-ab05b-default-rtdb.firebaseio.com/"
 
// Define Firebase Data object
FirebaseData fbdo;
FirebaseAuth auth;
FirebaseConfig config;
 
 
int intValue;
float floatValue;
bool signupOK = false;
 
const int LDRPIN = 34;
int LDR_VALUE = 0;
 
void setup()
{
 Serial.begin(9600);
 WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
 Serial.print("Connecting to Wi-Fi");
  while (WiFi.status() != WL_CONNECTED)
 {
   Serial.print(".");
   delay(300);
 }
  Serial.print("Connected with IP: ");
 Serial.println(WiFi.localIP());
 
 /* Assign the api key (required) */
 config.api_key = API_KEY;
 
 /* Assign the RTDB URL (required) */
 config.database_url = DATABASE_URL;
 
 /* Sign up */
 if (Firebase.signUp(&config, &auth, "", ""))
 {
   Serial.println("ok");
   signupOK = true;
 }
 else
 {
   Serial.printf("%s\n", config.signer.signupError.message.c_str());
 }
 
 /* Assign the callback function for the long running token generation task */
 config.token_status_callback = tokenStatusCallback; // see addons/TokenHelper.h
 
 Firebase.begin(&config, &auth);
 Firebase.reconnectWiFi(true);
}
 
void loop()
{
 LDR_VALUE=analogRead(LDRPIN);
 if (Firebase.ready() && signupOK )
 {
    if (Firebase.RTDB.setInt(&fbdo, "test/lightlevel", LDR_VALUE))
   {
     Serial.println("OK");
 
   }
   else
   {
     Serial.println("FAILED");
     Serial.println("REASON: " + fbdo.errorReason());
   }
 
 }
 delay(3000);
}

